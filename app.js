var express = require('express');
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser')
var pdf = require('express-pdf');

var http = require('http');
var path = require('path');
var config = require('config');
var log = require('libs/log')(module);
var mongoose = require('libs/mongoose');
var HttpError = require('error').HttpError;
var bodyParser = require('body-parser');
var cookeiParser = require('cookie-parser');
var favicon = require('serve-favicon');
var logger = require('morgan');
var limit = require('raw-body');

var multer  = require('multer');
var upload = multer({ dest: 'uploads/' });
var errorHandler = require('errorhandler');

var app = express();

app.set('port', config.get('port'));

app.engine('ejs', require('ejs-locals'))
app.set('views', path.join(__dirname, '/templates'));
app.set('view engine', 'ejs');

//app.use(pdf);

app.use(favicon(path.join(__dirname, 'public/images', 'logo.jpg')));
if (app.get('env') == 'development') {
    app.use(logger('dev'));
} else {
    app.use(logger('default'));
}

app.use( (req, res, next) => {
    limit(req, {
	limit: '5mb',
    }, (err, str) => {
	if (err) return next(err);
	req.text = str;
        next();
    });
});

app.use(express.json());

app.use(bodyParser.json({limit: '5mb'}));
app.use(bodyParser.urlencoded({limit: '5mb', extended: true}));
app.use(cookieParser());

var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
app.use(session({
  secret: config.get('session:secret'),
  key:    config.get('session:key'),
  cookie: config.get('session:cookie'),
  store: new MongoStore({mongooseConnection: mongoose.connection })
}));

// app.use(function(req, res, next) {
//    req.session.user = req.session.user + 1;
//    res.send("Visits:" + req.session.user);
// });

app.use(require("middleware/sendHttpError"));
app.use(require("middleware/loadUser"));
app.use(require("middleware/loadTeams"));

require('routes')(app);

app.use(express.static(path.join(__dirname, 'public')));



//error
app.use(function(err, req, res, next) {
  if (typeof err == 'number') {
    err = new HttpError(err);
  }

  if (err instanceof HttpError) {
    res.sendHttpError(err);
  } else {
//    if (app.get('env') == 'development') {
//      app.use(errorHandler());
//    } else {
      log.error(err);
      err = new HttpError(500);
      res.render("error", {error: err});
//      res.sendHttpError(err);
//    }
  }
});



http.createServer(app).listen(config.get('port'), function(){
  log.info('Express server listen on port ' + config.get('port'));
});
