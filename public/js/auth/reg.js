$(document.forms["registration-form"]).on('submit', function() {
  var form = $(this);

  $('div#div-reg-username').removeClass('has-error');
  $('div#div-reg-username').removeClass('has-success');

  $('.error',  form).html('');
  $(":submit", form).button("loading");

  $.ajax({
    url: "/registration",
    method: "POST",
    data: form.serialize(),
    complete: function() {
      $(":submit", form).button("reset");
    },
    statusCode: {
      200: function() {
        $('div#div-reg-username').addClass('has-success');
        window.location.href = "/dashboard";
      },
      403: function(jqXHR) {
        var error = JSON.parse(jqXHR.responseText);
        if (error.message == "user_incorrect"){
          $('div#div-reg-username').addClass('has-error');
        }
      }
    }
  });
  return false;
});
