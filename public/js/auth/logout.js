$(document.forms["form-logout"]).on('submit', function() {
  $.ajax({
    url: "/logout",
    method: "POST",
    data: "",
    statusCode: {
      200: function()      { window.location.href = "/";},
      403: function(jqXHR) {var error = JSON.parse(jqXHR.responseText);}
    }
  });
  return false;
});
