$(document.forms["addteam-form"]).on('submit', function() {
  var form = $(this);

  $('div#div-team_name').removeClass('has-success');
  $('div#div-team_name').removeClass('has-error');
  $('input#team_name').removeClass('alert-success');
  $('input#team_name').removeClass('alert-danger');

  $('.error',  form).html('');
  $(":submit", form).button("Loading...");

  var uri = "/admin/teams";
  if (role == "edit") {
    uri += "/editTeam/" + TID;
  } else {
    uri += "/addTeam";
  }

  $.ajax({
    url: uri,
    method: "POST",
    data: form.serialize(),
    complete: function() {
      $(":submit", form).button("reset");
    },
    statusCode: {
      200: function() {
        $('div#div-team_name').addClass('has-success');
        $('input#team_name').addClass('alert-success');
        window.location.href = "/admin/teams";
      },
      403: function(jqXHR) {
        var error = JSON.parse(jqXHR.responseText);
        if (error.message == "team_already_exist") {
          $('div#div-team_name').addClass('has-error');
          $('input#team_name').addClass('alert-danger');
          $('input#team_name').first().focus()
        }
      }
    }
  });

  return false;
});
