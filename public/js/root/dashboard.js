function clac(type, tid, mid) {

  if (type === "removeTeam") {
    $.ajax({
      url: "/admin/teams/removeTeam",
      method: "POST",
      data: {tid: tid},
      complete: function() {
        $(":submit", form).button("reset");
      },
      statusCode: {
        200: function() {
          window.location.href = "/admin/teams";
        },
        403: function(jqXHR) {
          var error = JSON.parse(jqXHR.responseText);
        },
        404: function(jqXHR) {
          var error = JSON.parse(jqXHR.responseText);
        }
      }
    });
  }

  if (type === "removeMember") {
    $.ajax({
      url: "/admin/teams/removeMember/",
      method: "POST",
      data: {tid: tid, mid: mid},
      complete: function() {
        $(":submit", form).button("reset");
      },
      statusCode: {
        200: function() {
          window.location.href = "/admin/teams";
        },
        403: function(jqXHR) {
          var error = JSON.parse(jqXHR.responseText);
        }
      }
    });
  }

}
