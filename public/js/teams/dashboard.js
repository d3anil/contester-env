function clac(type, tid, mid) {

  if (type === "removeTeam") {
    $.ajax({
      url: "/dashboard/removeTeam",
      method: "POST",
      data: {tid: tid},
      complete: function() {
        $(":submit", form).button("reset");
      },
      statusCode: {
        200: function() {
          window.location.href = "/dashboard";
        },
        403: function(jqXHR) {
          var error = JSON.parse(jqXHR.responseText);
        },
        404: function(jqXHR) {
          var error = JSON.parse(jqXHR.responseText);
        }
      }
    });
  }

  if (type === "removeMember") {
    $.ajax({
      url: "/dashboard/removeMember/",
      method: "POST",
      data: {tid: tid, mid: mid},
      complete: function() {
        $(":submit", form).button("reset");
      },
      statusCode: {
        200: function() {
          window.location.href = "/dashboard";
        },
        403: function(jqXHR) {
          var error = JSON.parse(jqXHR.responseText);
        }
      }
    });
  }

}
