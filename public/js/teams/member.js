$(document.forms["form-member"]).on('submit', function() {
  var form = $(this);

  $('div#div-name_eng').removeClass('has-success');
  $('div#div-name_eng').removeClass('has-error');
  $('input#name_eng').removeClass('alert-success');
  $('input#name_eng').removeClass('alert-danger');

  $('.error',  form).html('');
  $(":submit", form).button("Loading...");

  var uri = "/dashboard";
  if (role == "edit") {
    uri += "/team/" + TID + "/editMember/" + MID;
  } else {
    uri += "/team/" + TID + "/addMember";
  }

  $.ajax({
    url: uri,
    method: "POST",
    data: form.serialize(),
    complete: function() {
      $(":submit", form).button("reset");
    },
    statusCode: {
      200: function() {
        $('div#div-name_eng').addClass('has-success');
        $('input#name_eng').addClass('alert-success');
        window.location.href = "/dashboard";
      },
      403: function(jqXHR) {
        var error = JSON.parse(jqXHR.responseText);
        if (error.message == "member_already_exist") {
          $('div#div-name_eng').addClass('has-error');
          $('input#name_eng').addClass('alert-danger');
          $('input#name_eng').first().focus()
        }
      }
    }
  });

  return false;
});
