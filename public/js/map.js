ymaps.ready(init);

function init() {
  var hotelsMap = new ymaps.Map("map", {
    center: [56.363628, 41.311220],
    zoom: 12
  }, {
    searchControlProvider: 'yandex#search'
  });

  hotels.forEach( function(hotel) {
    var hint = hotel.name    + '<br>' +
               hotel.address + '<br>' +
               hotel.phone   + '<br>';
    if (hotel.site) hint += '<a href="' + hotel.site + '">'+ hotel.site + '</a><br>';
    hint += 'цена от: ' + hotel.cost + ' руб.';

    hotelsMap.geoObjects.add(new ymaps.Placemark( [hotel.coordinates.x, hotel.coordinates.y], {
      balloonContent: hint,
      iconCaption: hotel.name,
    }, {
      preset: 'islands#greenDotIconWithCaption'
    }));
  });

  hotelsMap.geoObjects.add(new ymaps.Placemark( [56.369235, 41.304147], {
  balloonContent: 'Вокзал',
  iconCaption: 'Вокзал',
  }, {
  preset: 'islands#redCircleDotIconWithCaption'
  }));

  hotelsMap.geoObjects.add(new ymaps.Placemark( [56.379235, 41.303578], {
  balloonContent: 'Место проведения',
  iconCaption: 'Место проведения (Бизнес-инкубатор)',
  }, {
  preset: 'islands#governmentCircleIcon'
  }));
}
