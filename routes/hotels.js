var Hotel = require('models/hotel').Hotel;

exports.get = function(req, res) {

  Hotel.find({}, function(err, hotels) {
    if (err) return next(err);
      res.render('hotels', {hotels: hotels});
  });
}
