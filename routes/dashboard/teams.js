var Team = require('models/team').Team;
var HttpError = require('error').HttpError;
var AuthError = require('error').AuthError;

// ===== ===== //
exports.getAddTeam = function(req, res) {
  res.render('dashboard/team', {flag: "add"});
}

// ===== ===== //
exports.postAddTeam = function(req, res, next) {
  var uid = req.user._id;
  var team = {
    team_name: req.body.team_name,
    team_IDE: req.body.team_IDE,
    organization: {
      fullname: req.body.org_fullname,
      name:     req.body.org_name,
      address:  req.body.org_address,
      email:    req.body.org_email,
      name_eng: req.body.org_name_eng,
    },
    coach: {
      name_eng: req.body.coach_name_eng,
      name_rus: req.body.coach_name_rus,
      post:     req.body.coach_post,
      address:  req.body.coach_address,
      email:    req.body.coach_email,
      phone:    req.body.coach_phone,
      passport: {
        series:     req.body.coach_passport_series,
        number:     req.body.coach_passport_number,
        birthDate:  req.body.coach_passport_birthDate,
        issued:     req.body.coach_passport_issued,
        issuedDate: req.body.coach_passport_issuedDate,
      }
    },
    creater: uid,
  }

  Team.addTeam(team, function(err) {
    if (err) {
      if (err instanceof AuthError) {
        return next(new HttpError(403, err.message));
      } else {
        return next(err);
      }
    }
    res.send({});
  });
};

// ===== ===== //
exports.getEditTeam = function(req, res, next) {
  var tid = req.params.tid;
  var uid = req.user._id;

  Team.findOne({_id: tid}, function(err, team) {
    if  (err) {
      if (err.name == "CastError")
        return next(new HttpError(400, "400. Неверный TID!  (⊙︿⊙)"));
      else
        return next(err);
    }
    if (!team) return next(new HttpError(404, "404. Такая команда не существует! =("));
    if (team.creater != uid) return next(new HttpError(403, "403. Нет доступа к этой команде! (ಠ_ಠ)"));

    res.render('dashboard/team', {flag: "edit", _tid: tid, team: team});
  });
}

// ===== ===== //
exports.postEditTeam = function(req, res, next) {
  var tid = req.params.tid;
  var uid = req.user._id;
  var newTeam = {
    team_name: req.body.team_name,
    team_IDE: req.body.team_IDE,
    organization: {
      fullname: req.body.org_fullname,
      name:     req.body.org_name,
      address:  req.body.org_address,
      email:    req.body.org_email,
      name_eng: req.body.org_name_eng,
    },
    coach: {
      name_eng: req.body.coach_name_eng,
      name_rus: req.body.coach_name_rus,
      post:     req.body.coach_post,
      address:  req.body.coach_address,
      email:    req.body.coach_email,
      phone:    req.body.coach_phone,
      passport: {
        series:     req.body.coach_passport_series,
        number:     req.body.coach_passport_number,
        birthDate:  req.body.coach_passport_birthDate,
        issued:     req.body.coach_passport_issued,
        issuedDate: req.body.coach_passport_issuedDate,
      }
    },
    creater: uid,
  }

  Team.editTeam(tid, uid, newTeam, function(err, ok) {
    if (err) {
      if (err instanceof AuthError) {
        if (err.message == "access_error")       return next(new HttpError(403, err.message));
        if (err.message == "team_already_exist") return next(new HttpError(403, err.message));
        if (err.message == "team_not_found")     return next(new HttpError(404, err.message));
        return next(err);
      } else {
        return next(err);
      }
    }
    res.send({});
  });
};

// ===== ===== //
exports.postRemoveTeam = function(req, res, next) {
  var tid = req.body.tid;
  var uid = req.user._id;

  Team.removeTeam(tid, uid, function (err) {
    if (err) {
      if (err instanceof AuthError) {
        if (err.message == "access_error")   return next(new HttpError(403, err.message));
        if (err.message == "team_not_found") return next(new HttpError(404, err.message));
        return next(err);
      } else {
        return next(err);
      }
    }
    res.send({});
  });
};

// ===== ===== //
exports.getAddMember = function(req, res) {
  var tid = req.params.tid;
  var uid = req.user.id;

  Team.findOne({_id: tid, creater: uid}, function(err, team) {
    if  (err) {
      if (err.name == "CastError")
        return next(new HttpError(400, "400. Неверный TID!  (⊙︿⊙)"));
      else
        return next(err);
    }
    if (!team) return next(new HttpError(404, "404. Такая команда не существует! =("));
    if (team.creater != uid) return next(new HttpError(403, "403. Нет доступа к этой команде! (ಠ_ಠ)"));

    res.render('dashboard/member', {flag: "add", _tid: tid, team: team});
  });

}

// ===== ===== //
exports.postAddMember = function(req, res, next) {
  var tid = req.params.tid;
  var uid = req.user._id;
  var newMember = {
    name_eng: req.body.name_eng,
    name_rus: req.body.name_rus,
    faculty:  req.body.faculty,
    group:    req.body.group,
    phone:    req.body.phone,
    year:     req.body.year,
    address:  req.body.address,
    email:    req.body.email,
    passport: {
      series:     req.body.passport_series,
      number:     req.body.passport_number,
      birthDate:  req.body.passport_birthDate,
      issued:     req.body.passport_issued,
      issuedDate: req.body.passport_issuedDate,
    }
  }

  Team.addMember(tid, uid, newMember, function(err) {
    if (err) {
      if (err instanceof AuthError) {
        if (err.message == "access_error")   return next(new HttpError(403, err.message));
        if (err.message == "team_not_found") return next(new HttpError(404, err.message));
        return next(err);
      } else {
        return next(err);
      }
    }
    res.send({});
  });
};

// ===== ===== //
exports.getEditMember = function(req, res, next) {
  var tid = req.params.tid;
  var mid = req.params.mid;
  var uid = req.user._id;

  Team.findOne(
    {"_id" : tid, "creater": uid},
    {"members": {"$elemMatch": {"_id": mid} } },
    function(err, team) {
      if (err) {
        if (err.name == "CastError")
          return next(new HttpError(400, "400. Неверный TID!  (⊙︿⊙)"));
        else
          return next(err);
      }
      if (!team)               return next(new HttpError(404, "404. Такая команда не существует! =("));
      if (!team.members[0])    return next(new HttpError(404, "404. Участник не найден"));
      return res.render('dashboard/member', {flag: "edit", _tid: tid, _mid: mid, member: team.members[0]});
    }
  );
}

// ===== ===== //
exports.postEditMember = function(req, res, next) {
  var tid = req.params.tid;
  var uid = req.user._id;
  var mid = req.params.mid;

  var member = {
    name_eng: req.body.name_eng,
    name_rus: req.body.name_rus,
    faculty:  req.body.faculty,
    group:    req.body.group,
    phone:    req.body.phone,
    year:     req.body.year,
    address:  req.body.address,
    email:    req.body.email,
    passport: {
      series:     req.body.passport_series,
      number:     req.body.passport_number,
      birthDate:  req.body.passport_birthDate,
      issued:     req.body.passport_issued,
      issuedDate: req.body.passport_issuedDate,
    }
  }

  // Team.editMember(tid, uid, member, function(err) {
  //   if (err) {
  //     if (err instanceof AuthError) {
  //       if (err.message == "not_found") return next(new HttpError(404, err.message));
  //       return next(err);
  //     } else {
  //       return next(err);
  //     }
  //   }
  //   res.send({});
  // });

  Team.removeMember(tid, uid, mid, function(err, ok) {
    if (err) return next(err);
    if (!ok) return next(err);
    Team.addMember(tid, uid, member, function(err) {
      if (err) {
        if (err instanceof AuthError) {
          if (err.message == "access_error")   return next(new HttpError(403, err.message));
          if (err.message == "team_not_found") return next(new HttpError(404, err.message));
          return next(err);
        } else {
          return next(err);
        }
      }
      res.send({});
    });
  });
};

// ===== ===== //
exports.postRemoveMember = function(req, res, next) {
  var tid = req.body.tid;
  var mid = req.body.mid;
  var uid = req.user._id;

  Team.removeMember(tid, uid, mid, function(err, ok) {
    if (err) return next(err);
    if (!ok) return next(err);
    res.send({});
  });
};
