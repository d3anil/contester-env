var Team = require('models/team').Team;

exports.get = function(req, res, next) {
  Team.find({creater: req.user._id}, function(err, teams) {
    if (err) return next(err);

    res.render('dashboard/dashboard', {teams: teams});
  });
}
