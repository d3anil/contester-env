var Agreement = require('models/agreement').Agreement;
var AuthError = require('error').AuthError;
var HttpError = require('error').HttpError;
var Path = require('path');
var fs = require('fs');

exports.get = function(req, res) {
  var uid = req.user._id;

  Agreement.find({creater: uid}, function(err, agreements) {
    if (err) return next(err);
    res.render('dashboard/personaldata', {agreements: agreements});
  });
}

exports.post = function(req, res, next) {
  var uid = req.user._id;
  var fil_oname = req.file.originalname.split(".");
  var fil_ext   = fil_oname[fil_oname.length-1];
  var fil_name  = req.file.filename;

  var fil_path = Path.join(__dirname, '../../uploads/', fil_name);
  var fil_npath = Path.join(__dirname, '../../agreements/', fil_name + '.' + fil_ext);
  // TODO проверку на существующий файл!
  fs.rename(fil_path, fil_npath, function(err) {
    if (err) return next(err);
    agreement = new Agreement({
      originalname: fil_oname,
      path: fil_npath,
      creater: uid,
    });
    agreement.save(function (err) {
      if (err) return next(err);
      res.redirect("/dashboard/personaldata");
    });
  });
}

exports.getAgreement = function(req, res, next) {
  var aid = req.params.aid;
  var uid = req.user.id;
  Agreement.findOne({_id: aid, creater: uid}, function(err, agreement) {
    if (err) return next(err);
    if (!agreement) return next(new HttpError(404, "Файл не найден"));
    res.sendfile(agreement.path);
  });
}

exports.postRemove = function(req, res, next) {
  var aid = req.params.aid;
  var uid = req.user.id;

  Agreement.findOne({_id: aid, creater: uid}, function(err, agreement) {
    if (err) return next(err);
    if (!agreement) return next(new HttpError(404, "Файл не найден"));
    // TODO проверку на существующий файл!
    fs.unlink(agreement.path, function(err) {if (err) return next(err);});
    Agreement.remove({_id: aid, creater: uid}, function (err) {
      if (err) callback(err);
      res.redirect("/dashboard/personaldata");
    });
  });
}
