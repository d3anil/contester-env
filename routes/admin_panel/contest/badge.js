var Team = require('models/team').Team;
var EJS = require('ejs');
var Path = require('path');

exports.get = function(req, res, next) {
  Team.find({}, function(err, teams) {
    if (err) return next(err);

    var path = Path.join(__dirname, '../../../templates/admin_panel/contest/badge.ejs');
    EJS.renderFile(path, {teams: teams}, function(err, html) {
      if (err) next(err);

      config = {
        // ЭТО внутри получается (100х70 мм)
        "width": "95mm",  // MAGIC!
        "height": "134mm", // MAGIC!

        "border": {
          "top": "0mm",           // default is 0, units: mm, cm, in, px
          "right": "0mm",
          "bottom": "0mm",
          "left": "0mm"
        },
        // File options
        "type": "pdf",             // allowed file types: png, jpeg, pdf
      }

      res.pdfFromHTML({
        filename: 'Badge.pdf',
        htmlContent: html,
        options: config
      });
    });

    // res.render('admin_panel/contest/certificate');
  });
}
