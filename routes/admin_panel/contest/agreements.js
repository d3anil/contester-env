var Team = require('models/team').Team;
var EJS = require('ejs');
var Path = require('path');

exports.get = function(req, res, next) {
  Team.find({}, function(err, teams) {
    if (err) return next(err);

    var path = Path.join(__dirname, '../../../templates/admin_panel/contest/agreements.ejs');
    EJS.renderFile(path, {teams: teams}, function(err, html) {
      if (err) next(err);

      config = {
        // ЭТО A4 внутри получается (210х297 мм)
        "width": "280mm",  // MAGIC!
        "height": "396mm", // MAGIC!

        "border": {
          "top": "20mm",           // default is 0, units: mm, cm, in, px
          "right": "20mm",
          "bottom": "20mm",
          "left": "20mm"
        },
        // File options
        "type": "pdf",             // allowed file types: png, jpeg, pdf
      }

      res.pdfFromHTML({
        filename: 'Badge.pdf',
        htmlContent: html,
        options: config
      });
    });

    // res.render('admin_panel/contest/certificate');
  });
}
