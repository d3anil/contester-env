var Team = require('models/team').Team;
var EJS = require('ejs');
var Path = require('path');

exports.get = function(req, res, next) {
  Team.find({}, function(err, teams) {
    if (err) return next(err);

    var path = Path.join(__dirname, '../../../templates/admin_panel/contest/certificate.ejs');
    EJS.renderFile(path, {teams: teams}, function(err, html) {
      if (err) next(err);

      config = {
        // ЭТО А4 внутри получается (210х297 мм)
        "width": "280mm",  // MAGIC!
        "height": "396mm", // MAGIC!

        "border": {
          "top": "0mm",           // default is 0, units: mm, cm, in, px
          "right": "0mm",
          "bottom": "0mm",
          "left": "0mm"
        },
        // File options
        "type": "pdf",             // allowed file types: png, jpeg, pdf
      }

      res.pdfFromHTML({
        filename: 'Certificates.pdf',
        htmlContent: html,
        options: config
      });
    });

    // res.render('admin_panel/contest/certificate');
  });
}
