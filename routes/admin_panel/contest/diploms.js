var Team = require('models/team').Team;
var EJS = require('ejs');
var Path = require('path');

exports.getSelect = function(req, res, next) {
  Team.find({}, function(err, teams) {
    if (err) return next(err);

    res.render('admin_panel/contest/select_diploms', {teams: teams});
  });
}

exports.get = function(req, res, next) {
  var query = req.query;
  var query_count = Object.keys(query).length;

  var teams = [];
  var counter = 0;
  for (key in query) {
    Team.findOne({_id: key}, function(err, team) {
      if (err) return next(err);
      teams.push(team);
      counter++;
      if (counter == query_count) {

        var path = Path.join(__dirname, '../../../templates/admin_panel/contest/diploms.ejs');
        EJS.renderFile(path, {teams: teams}, function(err, html) {
          if (err) next(err);

          config = {
            // ЭТО А4 внутри получается (210х297 мм)
            "width": "280mm",  // MAGIC!
            "height": "396mm", // MAGIC!

            "border": {
              "top": "0mm",           // default is 0, units: mm, cm, in, px
              "right": "0mm",
              "bottom": "0mm",
              "left": "0mm"
            },
            // File options
            "type": "pdf",             // allowed file types: png, jpeg, pdf
          }

          res.pdfFromHTML({
            filename: 'Diploms.pdf',
            htmlContent: html,
            options: config
          });
        });
      }
    });
  }

  // res.send({body:req.body, query:req.query});
}
