var User = require('models/user').User;
var Admins = require('models/admins').Admins;

exports.get = function(req, res, next) {

  User.find({}, function(err, users) {
    if (err) return next(err);

    var resp = [];
    var counter = 0;
    var l = users.length;
    users.forEach( function(user, i, arr) {
      var dat = new Date(user.created);
      Admins.findOne({id: user._id}, function(err, admin) {
        if (err) return next(err);
        resp.push({
          "username": user.username,
          "id": user._id,
          "isAdmin": (admin) ? true : false,
        });
        counter++;
        if (counter == l) {
          res.render('admin_panel/admins', {users: resp});
        }
      });
    });
  })
}

exports.post = function(req, res, next) {
  uid = req.params.uid;

  Admins.findOne({id: uid}, function(err, admin) {
    if (err) return next(err);
    if (admin) {
      Admins.remove({id: uid}, function(err) {
        if (err) return next(err);
        res.send({});
      });
    } else {
      var newAdmin = new Admins({id: uid});
      newAdmin.save(function(err) {
        if (err) return next(err);
        res.send({});
      });
    }
  });
}
