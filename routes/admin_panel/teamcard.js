var Team = require('models/team').Team;
var EJS = require('ejs');
var Path = require('path');

exports.get = function(req, res, next) {
  Team.find({}, function(err, teams) {
    if (err) return next(err);

    var path = Path.join(__dirname, '../../templates/admin_panel/teamcard.ejs');
    EJS.renderFile(path, {teams: teams}, function(err, html) {
      if (err) next(err);

      config = {
        "format": "A4",            // allowed units: A3, A4, A5, Legal, Letter, Tabloid
        "orientation": "portrait", // portrait or landscape

        "border": {
          "top": "10mm",           // default is 0, units: mm, cm, in, px
          "right": "10mm",
          "bottom": "20mm",
          "left": "10mm"
        },
        // File options
        "type": "pdf",             // allowed file types: png, jpeg, pdf
      }

      res.pdfFromHTML({
        filename: 'Team_cards.pdf',
        htmlContent: html,
        options: config
      });
    });
  });
}
