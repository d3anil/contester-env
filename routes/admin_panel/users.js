var User = require('models/user').User;

exports.get = function(req, res) {

  User.find({}, function(err, users) {
    if (err) return next(err);

    var resp = [];
    users.forEach( function(user, i, arr) {
      var dat = new Date(user.created);
      resp.push({
        "username": user.username,
        "id": user._id,
        "email": user.email,
        "created (TMZ: Europe/Moscow)": dat.toLocaleString({timeZone: "Europe/Moscow"}),
      });
    });
    res.render('admin_panel/users', {reg_users: JSON.stringify(resp, null, "    ")});
  })
}
