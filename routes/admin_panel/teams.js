var Team = require('models/team').Team;
var HttpError = require('error').HttpError;
var AuthError = require('error').AuthError;

exports.get = function(req, res, next) {
  Team.find({}, function(err, teams) {
    if (err) return next(err);

    res.render('admin_panel/teams', {teams: teams});
  });
}

// ===== ===== //
exports.getEditTeam = function(req, res, next) {
  var tid = req.params.tid;

  Team.findOne({_id: tid}, function(err, team) {
    if  (err) {
      if (err.name == "CastError")
        return next(new HttpError(400, "400. Неверный TID!  (⊙︿⊙)"));
      else
        return next(err);
    }
    if (!team) return next(new HttpError(404, "404. Такая команда не существует! =("));

    res.render('admin_panel/team', {flag: "edit", _tid: tid, team: team});
  });
}

// ===== ===== //
exports.postEditTeam = function(req, res, next) {
  var tid = req.params.tid;
  var newTeam = {
    team_name: req.body.team_name,
    team_IDE: req.body.team_IDE,
    organization: {
      fullname: req.body.org_fullname,
      name:     req.body.org_name,
      address:  req.body.org_address,
      email:    req.body.org_email,
      name_eng: req.body.org_name_eng,
    },
    coach: {
      name_eng: req.body.coach_name_eng,
      name_rus: req.body.coach_name_rus,
      post:     req.body.coach_post,
      address:  req.body.coach_address,
      email:    req.body.coach_email,
      phone:    req.body.coach_phone,
      passport: {
        series:     req.body.coach_passport_series,
        number:     req.body.coach_passport_number,
        birthDate:  req.body.coach_passport_birthDate,
        issued:     req.body.coach_passport_issued,
        issuedDate: req.body.coach_passport_issuedDate,
      }
    },
  }

  Team.editTeam_root(tid, newTeam, function(err, ok) {
    if (err) {
      if (err instanceof AuthError) {
        if (err.message == "team_already_exist") return next(new HttpError(403, err.message));
        if (err.message == "team_not_found")     return next(new HttpError(404, err.message));
        return next(err);
      } else {
        return next(err);
      }
    }
    res.send({});
  });
};

// ===== ===== //
exports.postRemoveTeam = function(req, res, next) {
  var tid = req.body.tid;

  Team.removeTeam_root(tid, function (err) {
    if (err) {
      if (err instanceof AuthError) {
        if (err.message == "team_not_found") return next(new HttpError(404, err.message));
        return next(err);
      } else {
        return next(err);
      }
    }
    res.send({});
  });
};

// ===== ===== //
exports.getEditMember = function(req, res, next) {
  var tid = req.params.tid;
  var mid = req.params.mid;

  Team.findOne(
    {"_id" : tid},
    {"members": {"$elemMatch": {"_id": mid} } },
    function(err, team) {
      if (err) {
        if (err.name == "CastError")
          return next(new HttpError(400, "400. Неверный TID!  (⊙︿⊙)"));
        else
          return next(err);
      }
      if (!team)               return next(new HttpError(404, "404. Такая команда не существует! =("));
      if (!team.members[0])    return next(new HttpError(404, "404. Участник не найден"));
      return res.render('admin_panel/member', {flag: "edit", _tid: tid, _mid: mid, member: team.members[0]});
    }
  );
}

// ===== ===== //
exports.postEditMember = function(req, res, next) {
  var tid = req.params.tid;
  var mid = req.params.mid;

  var member = {
    name_eng: req.body.name_eng,
    name_rus: req.body.name_rus,
    faculty:  req.body.faculty,
    group:    req.body.group,
    phone:    req.body.phone,
    year:     req.body.year,
    address:  req.body.address,
    email:    req.body.email,
    passport: {
      series:     req.body.passport_series,
      number:     req.body.passport_number,
      birthDate:  req.body.passport_birthDate,
      issued:     req.body.passport_issued,
      issuedDate: req.body.passport_issuedDate,
    }
  }

  Team.removeMember_root(tid, mid, function(err, ok) {
    if (err) return next(err);
    if (!ok) return next(err);
    Team.addMember_root(tid, member, function(err) {
      if (err) {
        if (err instanceof AuthError) {
          if (err.message == "team_not_found") return next(new HttpError(404, err.message));
          return next(err);
        } else {
          return next(err);
        }
      }
      res.send({});
    });
  });
};

// ===== ===== //
exports.postRemoveMember = function(req, res, next) {
  var tid = req.body.tid;
  var mid = req.body.mid;

  Team.removeMember_root(tid, mid, function(err, ok) {
    if (err) return next(err);
    if (!ok) return next(err);
    res.send({});
  });
};
