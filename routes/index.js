var checkAuth  = require('middleware/checkAuth');
var checkAdmin = require('middleware/checkAdmin');
var inDev = require('middleware/inDev');

var multer  = require('multer');
var upload = multer({ dest: 'uploads/' });

module.exports = function(app) {

  console.log("ROUTER!");

  app.get('/', require('./frontpage').get);

  app.get ('/registration', require('./registration').get);
  app.post('/registration', require('./registration').post);
  app.post('/login',  inDev, require('./login').post);

  app.post('/logout', require('./logout').post);

  app.get ('/dashboard',               checkAuth, require('./dashboard').get);
  app.get ('/dashboard/addTeam',       checkAuth, require('./dashboard/teams').getAddTeam);
  app.post('/dashboard/addTeam',       checkAuth, require('./dashboard/teams').postAddTeam);
  app.get ('/dashboard/editTeam/:tid', checkAuth, require('./dashboard/teams').getEditTeam);
  app.post('/dashboard/editTeam/:tid', checkAuth, require('./dashboard/teams').postEditTeam);
  app.post('/dashboard/removeTeam',    checkAuth, require('./dashboard/teams').postRemoveTeam)
  app.get ('/dashboard/team/:tid/addMember', checkAuth, require('./dashboard/teams').getAddMember);
  app.post('/dashboard/team/:tid/addMember', checkAuth, require('./dashboard/teams').postAddMember);
  app.get ('/dashboard/team/:tid/editMember/:mid',   checkAuth, require('./dashboard/teams').getEditMember);
  app.post('/dashboard/team/:tid/editMember/:mid',   checkAuth, require('./dashboard/teams').postEditMember);
  app.post('/dashboard/removeMember/',               checkAuth, require('./dashboard/teams').postRemoveMember);
  app.get ('/dashboard/personaldata', checkAuth, require('./dashboard/personaldata').get);
  app.post('/dashboard/personaldata', checkAuth, upload.single('agreement'), require('./dashboard/personaldata').post);
  app.get ('/dashboard/agreement/:aid', checkAuth, require('./dashboard/personaldata').getAgreement);
  app.post('/dashboard/agreement/:aid/remove', checkAuth, require('./dashboard/personaldata').postRemove);

  app.get('/final_tables/2017', require('./final_tables/2017').get);
  app.get('/final_tables/2018', require('./final_tables/2018').get);

  app.get ('/regulations', require('./regulations').get);
  app.get ('/hotels', require('./hotels').get);

  app.get ('/admin/users',       checkAuth, checkAdmin, require('./admin_panel/users').get);
  app.get ('/admin/admins',      checkAuth, checkAdmin, require('./admin_panel/admins').get);
  app.post('/admin/admins/:uid', checkAuth, checkAdmin, require('./admin_panel/admins').post);

  app.get ('/admin/teamcard',            checkAuth, checkAdmin, require('./admin_panel/teamcard').get);
  app.get ('/admin/contest/certificate', checkAuth, checkAdmin, require('./admin_panel/contest/certificate').get);
  app.get ('/admin/contest/diploms',     checkAuth, checkAdmin, require('./admin_panel/contest/diploms').getSelect);
  app.get ('/admin/contest/gen_diploms',     checkAuth, checkAdmin, require('./admin_panel/contest/diploms').get);
  app.get ('/admin/contest/badge',     checkAuth, checkAdmin, require('./admin_panel/contest/badge').get);
  app.get ('/admin/contest/agreements',     checkAuth, checkAdmin, require('./admin_panel/contest/agreements').get);


  app.get ('/admin/teams',               checkAuth, checkAdmin, require('./admin_panel/teams').get);
  app.get ('/admin/teams/editTeam/:tid', checkAuth, checkAdmin, require('./admin_panel/teams').getEditTeam);
  app.post('/admin/teams/editTeam/:tid', checkAuth, checkAdmin, require('./admin_panel/teams').postEditTeam);
  app.post('/admin/teams/removeTeam',    checkAuth, checkAdmin, require('./admin_panel/teams').postRemoveTeam);
  app.get ('/admin/teams/team/:tid/editMember/:mid',   checkAuth, checkAdmin, require('./admin_panel/teams').getEditMember);
  app.post('/admin/teams/team/:tid/editMember/:mid',   checkAuth, checkAdmin, require('./admin_panel/teams').postEditMember);
  app.post('/admin/teams/removeMember/',               checkAuth, checkAdmin, require('./admin_panel/teams').postRemoveMember);
};
