var passport = require("passport");
var VKontakteStrategy = require('passport-vkontakte').Strategy;

passport.use(new VKontakteStrategy(
  {
    clientID:     "5925255", // VK.com docs call it 'API ID', 'app_id', 'api_id', 'client_id' or 'apiId'
    clientSecret: "VNAoHEawNLVsKZWAc8CK",
    callbackURL:  "http://localhost:3000/auth/vkontakte/callback"
  },
  function myVerifyCallbackFn(accessToken, refreshToken, params, profile, done) {
    console.log(arguments);
    // Now that we have user's `profile` as seen by VK, we can
    // use it to find corresponding database records on our side.
    // Also we have user's `params` that contains email address (if set in
    // scope), token lifetime, etc.
    // Here, we have a hypothetical `User` class which does what it says.
    User.findOrCreate({ vkontakteId: profile.id })
        .then(function (user) { done(null, user); })
        .catch(done);
  }
));
