
var rooms = [];
rooms.push({num: "2.47", pc_count: 4, teams: []});
rooms.push({num: "2.48", pc_count: 2, teams: []});
rooms.push({num: "2.49", pc_count: 3, teams: []});
rooms.push({num: "1.37", pc_count: 2, teams: []});
rooms.push({num: "1.17", pc_count: 4, teams: []});

var teams = [];
/*  1 */teams.push({name: "Kovrov-Signal",  city: 0, room: "", pass: ""});
/*  2 */teams.push({name: "KSTA#2",         city: 0, room: "", pass: ""});
/*  3 */teams.push({name: "KSTA#3",         city: 0, room: "", pass: ""});
/*  4 */teams.push({name: "OrelSU_1",       city: 1, room: "", pass: ""});
/*  5 */teams.push({name: "OrelSU_2",       city: 1, room: "", pass: ""});
/*  6 */teams.push({name: "Vyatka_SU",      city: 2, room: "", pass: ""});
/*  7 */teams.push({name: "ISPU_#1",        city: 3, room: "", pass: ""});
/*  8 */teams.push({name: "RSATA",          city: 4, room: "", pass: ""});
/*  9 */teams.push({name: "MIPT_MUR",       city: 5, room: "", pass: ""});
/* 10 */teams.push({name: "MIPT_MMS",       city: 5, room: "", pass: ""});
/* 11 */teams.push({name: "MIPT_Qwarks",    city: 5, room: "", pass: ""});
/* 12 */teams.push({name: "Raduga",         city: 6, room: "", pass: ""});
/* 13 */teams.push({name: "Yaroslavl_SU_1", city: 7, room: "", pass: ""});


function pas_gen() {
  var words = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z",
               "a","b","c","d","e","f","g","h","i","j","k",    "m","n","o","p","q","r","s","t","u","v","w","x","y","z",
                      ,"2","3","4","5","6","7","8","9"]

  var pas = "";
  for (i=0; i<8; i++) {
    var num = (Math.random() * words.length) | 0;
    pas += words[num];
  }

  return pas;
}

var completed = 0;

while (completed < teams.length) {
  var room = (Math.random() * rooms.length) | 0;

  var arr = rooms[room].teams;
  var overlap = false;

  if (arr.length < rooms[room].pc_count) {
    for (i=0; i<arr.length; i++) {
      if (teams[completed].city == teams[i].city) {
        overlap = true;
        //break;
      }
    }
  } else {
    overlap = true;
  }

  if (!overlap) {
    rooms[room].teams.push(completed);
    teams[completed].room = room;
    teams[completed].pass = pas_gen();
    console.log(teams[completed].name+";"+teams[completed].pass+";"+rooms[teams[completed].room].num);
    completed++;
  }
}

console.log("===========================");
console.log(rooms);
console.log("===========================");
