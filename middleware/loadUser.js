var User = require('models/user').User;
var Admins = require('models/admins').Admins;

module.exports = function(req, res, next) {
  console.log("===== MIDDLEWARE LOAD USER =====");
  req.user = res.locals.user = null;
  if (!req.session.user) {
    console.log("Session not found, new user -  return");
    return next();
  }

  User.findById(req.session.user, function(err, user) {
    console.log("Search existing user...");
    if (err) return next(err);

    user.isAdmin = false;
    if (user.username == "admin") {
        user.isAdmin = true;
        req.user = res.locals.user = user;
        next();
    } else {
      Admins.findOne({id: user._id}, function (err, admin) {
        if (admin) {
          user.isAdmin = true;
        }
        req.user = res.locals.user = user;
        next();
      })
    }
  });
};
