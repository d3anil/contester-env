var HttpError = require('error').HttpError;

module.exports = function(req, res, next) {
  if (req.user.isAdmin) {
    next();
  } else {
    return next(new HttpError(403, "У Вас не достаточно прав!"));
  }
}
