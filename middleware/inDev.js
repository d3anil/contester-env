var HttpError = require('error').HttpError;

module.exports = function(req, res, next) {
  if (!req.user) {
    return next(new HttpError(403, "Раздел находится в стадии разработки!"));
  }
  if (req.user.isAdmin) {
    next();
  }
  return next(new HttpError(403, "Раздел находится в стадии разработки!"));
}
