var crypto = require('crypto');
var async = require('async');
var util = require('util');

var mongoose = require('libs/mongoose'),
Schema = mongoose.Schema;

var schema = new Schema({
  originalname: {
    type: String,
  },
  path: {
    type: String,
    required: true,
  },
  creater: {
    type: String,
    required: true,
  }
});

exports.Agreement = mongoose.model('Agreement', schema);
