var crypto = require('crypto');
var async = require('async');
var util = require('util');

var mongoose = require('libs/mongoose'),
Schema = mongoose.Schema;

var coordinatesSchema = new Schema({ name: String });

var schema = new Schema({
  name: {
    type: String,
    required: true,
  },
  address: {
    type: String,
    default: null,
  },
  phone: {
    type: String,
    default: null,
  },
  site: {
    type: String,
    default: null,
  },
  cost: {
    type: String,
    default: null,
  },
  coordinates: {
    x: {
      type: Number,
      required: true,
    },
    y: {
      type: Number,
      required: true,
    },
  },
});


schema.statics.add = function(hotel, callback) {
  var Hotel = this;

  var hotel = new Hotel(hotel);
  hotel.save(function(err) {
    if (err) return callback(err);
    callback(null);
  });
}

exports.Hotel = mongoose.model('Hotel', schema);
