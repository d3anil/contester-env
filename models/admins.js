var crypto = require('crypto');
var async = require('async');
var util = require('util');

var mongoose = require('libs/mongoose'),
Schema = mongoose.Schema;

var schema = new Schema({
  id: {
    type: Schema.Types.ObjectId,
    unique: true,
    required: true,
  }
});

exports.Admins = mongoose.model('Admin', schema);
