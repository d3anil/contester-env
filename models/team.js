var crypto = require('crypto');
var async = require('async');
var AuthError = require('error').AuthError;

var mongoose = require('libs/mongoose'),
Schema = mongoose.Schema;

var sch_organization = new Schema({
  fullname: {type: String},
  name:     {type: String},
  address:  {type: String},
  email:    {type: String},
  name_eng: {type: String},
});

var sch_passport = new Schema({
  series:     {type: String},
  number:     {type: String},
  birthDate:  {type: String},
  issued:     {type: String},
  issuedDate: {type: String}
});

var sch_coach = new Schema({
  name_eng: {type: String},
  name_rus: {type: String},
  post:     {type: String},
  address:  {type: String},
  email:    {type: String},
  phone:    {type: String},
  passport: sch_passport
});

var sch_member = new Schema({
  name_eng: {
    type: String,
    required: true
  },
  name_rus: {type: String},
  faculty:  {type: String},
  group:    {type: String},
  year:     {type: String},
  address:  {type: String},
  email:    {type: String},
  phone:    {type: String},
  passport: sch_passport,
});

var schema = new Schema({
  "team_name": {
    type: String,
    required: true
  },
  "team_IDE": {
    type: String,
  },
  organization: sch_organization,
  coach: sch_coach,
  members: [sch_member],

  "creater":{
    type: String,
    required: true
  },
  "created": {
    type: Date,
    default: Date.now
  }
});

// ===== ===== //
schema.statics.addTeam = function(teamData, callback) {
  var Team = this;

  async.waterfall([
    function(callback) {
        Team.findOne({team_name: teamData.team_name}, callback);
    },
    function(findTeam, callback) {
      if (findTeam) {
        callback(new AuthError("team_already_exist"));
      } else {
        var newTeam = new Team(teamData);
        newTeam.save(function(err) {
          if (err) return callback(err);
          callback(null, newTeam);
        });
      }
    }
  ], callback);
}

// ===== ===== //
schema.statics.editTeam = function(tid, uid, newTeam, callback) {
  var Team = this;

  Team.findOne({_id: tid}, function(err, findTeam) {
    if (err)       return callback(err);
    if (!findTeam) return callback(new AuthError("team_not_found"));
    if (findTeam.creater != uid) return callback(new AuthError("access_error"));

    if (findTeam.team_name != newTeam.team_name) {
      Team.findOne({team_name: newTeam.team_name}, function(err, findTeam) {
        if (findTeam) return callback(new AuthError("team_already_exist"));
          Team.update({_id: tid, creater: uid}, {$set: newTeam}, null, callback);
      });
    } else {
      Team.update({_id: tid, creater: uid}, {$set: newTeam}, null, callback);
    }
  });
}

// ===== ===== //
schema.statics.removeTeam = function(tid, uid, callback) {
  var Team = this;

  async.waterfall([
    function(callback) {
        Team.findOne({_id: tid}, callback);
    },
    function(findTeam, callback) {
      if (!findTeam)               callback(new AuthError("team_not_found"));
      if (findTeam.creater != uid) callback(new AuthError("access_error"));

      Team.remove({_id: tid, creater: uid}, function (err) {
        if (err) callback(err);
        callback(null);
      });
    }
  ], callback);
}

// ===== ===== //
schema.statics.addMember = function(tid, uid, newMember, callback) {
  var Team = this;

  async.waterfall([
    function(callback) {
        Team.findOne({_id: tid}, callback);
    },
    function(findTeam, callback) {
      if (!findTeam)               callback(new AuthError("team_not_found"));
      if (findTeam.creater != uid) callback(new AuthError("access_error"));

      Team.update({_id: tid, creater: uid}, {$push: {"members": newMember}}, null, callback);
    }
  ], callback);
}

// ===== ===== //
schema.statics.removeMember = function(tid, uid, mid, callback) {
  var Team = this;

  async.waterfall([
    function(callback) {
        Team.findOne({_id: tid}, callback);
    },
    function(findTeam, callback) {
      if (!findTeam)               callback(new AuthError("team_not_found"));
      if (findTeam.creater != uid) callback(new AuthError("access_error"));

      Team.update({_id: tid, creater: uid}, {$pull: {"members": {"_id": mid}} }, null, callback);
    }
  ], callback);
}

// ===== ===== //
schema.statics.editMember = function(tid, uid, member, callback) {
  var Team = this;

  async.waterfall([
    function(callback) {
        Team.findOne({_id: tid, creater: uid, "members._id": member.id}, callback);
    },
    function(findTeam, callback) {
      if (!findTeam) callback(new AuthError("not_found"));
      Team.update({_id: tid, creater: uid, "members._id": member.id}, {$set: {"members.$": member} }, null, function(err, some) {
        if (err) callback(err);
        callback(err);
      });
    }
  ], callback);
}

// ===== ===== //
// ===== ===== //
// ===== ===== //
// ===== ===== //

schema.statics.removeMember_root = function(tid, mid, callback) {
  var Team = this;

  async.waterfall([
    function(callback) {
        Team.findOne({_id: tid}, callback);
    },
    function(findTeam, callback) {
      if (!findTeam)               callback(new AuthError("team_not_found"));

      Team.update({_id: tid}, {$pull: {"members": {"_id": mid}} }, null, callback);
    }
  ], callback);
}

schema.statics.editMember_root = function(tid, member, callback) {
  var Team = this;

  async.waterfall([
    function(callback) {
        Team.findOne({_id: tid, "members._id": member.id}, callback);
    },
    function(findTeam, callback) {
      if (!findTeam) callback(new AuthError("not_found"));
      Team.update({_id: tid, "members._id": member.id}, {$set: {"members.$": member} }, null, function(err, some) {
        if (err) callback(err);
        callback(err);
      });
    }
  ], callback);
}

// ===== ===== //
schema.statics.editTeam_root = function(tid, newTeam, callback) {
  var Team = this;

  Team.findOne({_id: tid}, function(err, findTeam) {
    if (err)       return callback(err);
    if (!findTeam) return callback(new AuthError("team_not_found"));

    if (findTeam.team_name != newTeam.team_name) {
      Team.findOne({team_name: newTeam.team_name}, function(err, findTeam) {
        if (findTeam) return callback(new AuthError("team_already_exist"));
          Team.update({_id: tid}, {$set: newTeam}, null, callback);
      });
    } else {
      Team.update({_id: tid}, {$set: newTeam}, null, callback);
    }
  });
}

// ===== ===== //
schema.statics.removeTeam_root = function(tid, callback) {
  var Team = this;

  async.waterfall([
    function(callback) {
        Team.findOne({_id: tid}, callback);
    },
    function(findTeam, callback) {
      if (!findTeam)               callback(new AuthError("team_not_found"));

      Team.remove({_id: tid}, function (err) {
        if (err) callback(err);
        callback(null);
      });
    }
  ], callback);
}

// ===== ===== //
schema.statics.addMember_root = function(tid, newMember, callback) {
  var Team = this;

  async.waterfall([
    function(callback) {
        Team.findOne({_id: tid}, callback);
    },
    function(findTeam, callback) {
      if (!findTeam)               callback(new AuthError("team_not_found"));

      Team.update({_id: tid}, {$push: {"members": newMember}}, null, callback);
    }
  ], callback);
}

exports.Team = mongoose.model('Team', schema);
