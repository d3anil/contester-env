var mongoose = require('libs/mongoose');
mongoose.set('debug', true);
var async = require('async');

async.series([
  open,
  // dropDatabase,
  requireModels,
  createRecords,
], function(err, results) {
  console.log(JSON.stringify(arguments, "", 4));
  closeDb( function(err) {
    if (err) console.log(err);
  });
  process.exit(err? 255 : 0);
});

function open(callback) {
  console.log("openning");
  mongoose.connection.on('open', callback);
}

function dropDatabase(callback) {
  console.log("dropingDb");
  var db = mongoose.connection.db;
  db.dropCollection('hotels', callback);
}

function requireModels(callback) {
  console.log("requiring Models");
  require('models/hotel');

  async.each(Object.keys(mongoose.models), function(modelName, callback) {
    mongoose.models[modelName].ensureIndexes(callback);
  }, callback);
}

function createRecords(callback) {
  var hotels = [
    {
    "name": "Гостиница Ковров",
    "address": "ул. Урицкого, 14",
    "phone": "8(49232)2-12-15",
    "site": null,
    "cost": 500,
    "coordinates": {x:56.376391, y:41.305687}
    },
    {
    "name": "Гостиница \"Старый - город\"",
    "address": "ул. Абельмана 1, стр. 1",
    "phone": "8(49232)4-65-55, 4-63-33, 4-65-45",
    "site": "http://restgroup.ru/",
    "cost": 1800,
    "coordinates": {x:56.370454, y:41.305669}
    },
    {
    "name": "Гостиница \"Визит\"",
    "address": "ул. Димитрова, 59",
    "phone": "8(49232)9-42-06",
    "site": "http://vizit-kovrov.ru/",
    "cost": 2500,
    "coordinates": {x:56.360576, y:41.337505}
    },
    {
    "name": "Гостиница \"Agora\"",
    "address": "ул. Абельмана, д.3",
    "phone": "8(49232)4-83-04, 8-915-753-88-90",
    "site": "http://kovrov-agora.ru/",
    "cost": 	1300,
    "coordinates": {x:56.370942, y:41.304860}
    },
    {
    "name": "Гостиница \"Эльотель\"",
    "address": "ул. Никонова, 43",
    "phone": "8(49232)4-89-00",
    "site": "http://elotel.ru/",
    "cost": 2500,
    "coordinates": {x:56.381744, y:41.324551}
    },
    {
    "name": "Гостиница \"АтриумМини\"",
    "address": "ул. Кирова, 138",
    "phone": "8(49232)6-43-12",
    "site": "http://atrium-resort.ru/",
    "cost": 3800,
    "coordinates": {x:56.345234, y:41.290837}
    },
    {
    "name": "Гостиница \"Palazzo\"",
    "address": "проезд Тургенева, 3",
    "phone": "8(49232)4-31-32, 8-(904)-038-28-23",
    "site": "http://www.palazzo-kovrov.ru/",
    "cost": 3000,
    "coordinates": {x:56.351574, y:41.318047}
    },
    {
    "name": "Гостиница \"Диана\"",
    "address": "ул. Строителей, 13/1",
    "phone": "8(49232)6-93-94",
    "site": "http://di-hotel.ru/",
    "cost": 1900,
    "coordinates": {x:56.336843, y:41.300503}
    },
    {
    "name": "Мини-гостиница Хостел",
    "address": "ул. Строителей, 33",
    "phone": "8-(900)-588-12-12",
    "site": "http://hostelparus.ru/",
    "cost": 1299,
    "coordinates": {x:56.336274, y:41.311571}
    },
    {
    "name": "Хостел \"Домино\"",
    "address": "ул. Парковая, 2",
    "phone": "8-(900)-590-16-77",
    "site": "http://hostelkovrov.ru/",
    "cost": 1299,
    "coordinates": {x:56.355894, y:41.335268}
    }
  ]

  console.log("creating");
  async.each(hotels, function(recordData, callback) {
    var record = new mongoose.models.Hotel(recordData);
    record.save(callback);
  }, callback);

}

function closeDb(callback) {
  console.log("closing");
  mongoose.disconnect(callback);
}
