#!/bin/sh

sudo npm i -g bower --allow-root

mkdir ./agreements
mkdir ./uploads
mkdir ./public/vendor/
cd ./public/vendor/
bower i jquery bootstrap --allow-root

cd ../../
echo 'NODE_PATH=. NODE_ENV=development node ./app.js ' > run-dev.sh
chmod +x ./run-dev.sh
